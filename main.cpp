
/*
 * SOFTWARE:
 * 		MusicRandomizer
 * AUTHOR:
 * 		Theophile BASTIAN 'Tobast' <contact@tobast.fr>
 * ERROR REPORTING:
 * 		<error-report@tobast.fr>
 *
 * DESCRIPTION:
 * 		Small program designed to "randomize" the playing order of music tracks on a player without "random" feature
 * 		Prepends random IDs to each track and folder in order to alter alphabetic order.
 *
 * LICENCE:
 *		GNUGPLv3:
 *
 * 		Copyright (C) 2013 Theophile BASTIAN
 *		This program is free software: you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation, either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QTextCodec>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>

#include <cstdlib> // rand()
#include <ctime>

#include "GraphicalCore.h"

int main(int argc, char** argv)
{
	srand(time(NULL));

	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf-8")); // Strings in files are UTF-8 encoded

	QApplication a(argc, argv);

	QString locale = QLocale::system().name().section('_', 0, 0); // Installs a translator to the user's locale
	QTranslator translator;
	translator.load(QString("qt_") + locale, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	translator.load(QString(":translate/") + locale);
	a.installTranslator(&translator);

	GraphicalCore c;
	c.show();

	return a.exec();
}

