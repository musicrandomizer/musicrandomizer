
/*
 * SOFTWARE:
 * 		MusicRandomizer
 * AUTHOR:
 * 		Theophile BASTIAN 'Tobast' <contact@tobast.fr>
 * ERROR REPORTING:
 * 		<error-report@tobast.fr>
 *
 * DESCRIPTION:
 * 		Small program designed to "randomize" the playing order of music tracks on a player without "random" feature
 * 		Prepends random IDs to each track and folder in order to alter alphabetic order.
 *
 * LICENCE:
 *		GNUGPLv3:
 *
 * 		Copyright (C) 2013 Theophile BASTIAN
 *		This program is free software: you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation, either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GraphicalCore.h"

GraphicalCore::GraphicalCore(QWidget* parent) :
	QMainWindow(parent)
{
	buildWidget();
}

void GraphicalCore::buildWidget()
{
	setWindowIcon(QIcon(":icon/24px"));
	setWindowTitle("Music Randomizer");

	centralArea = new QWidget;
	layoutMain = new QVBoxLayout;
	
	pathGroupbox = new QGroupBox(tr("Directory"));
	pathGroupbox->setFlat(true);
	layoutMain->addWidget(pathGroupbox);
	layoutPath = new QHBoxLayout;
	le_dirpath = new QLineEdit();
	le_dirpath->setReadOnly(true);
	layoutPath->addWidget(le_dirpath);
	pb_selectPath = new QPushButton(tr("Choose"));
	layoutPath->addWidget(pb_selectPath);
	pathGroupbox->setLayout(layoutPath);
	layoutMain->addWidget(pathGroupbox);

	cb_recursive = new QCheckBox(tr("Affect subdirectories"));
	cb_recursive->setChecked(true);
	layoutMain->addWidget(cb_recursive);
	
	// Add potential checkboxes here.
	
	pb_randomize = new QPushButton(tr("Randomize"));
	layoutMain->addWidget(pb_randomize);

	pb_unrandomize = new QPushButton(tr("Remove random tags"));
	layoutMain->addWidget(pb_unrandomize);

	centralArea->setLayout(layoutMain);
	setCentralWidget(centralArea);
	
	// CONNECTIONS
	connect(pb_selectPath, SIGNAL(clicked()), this, SLOT(slotSelectPath()));
	connect(pb_randomize, SIGNAL(clicked()), this, SLOT(randomize()));
	connect(pb_unrandomize, SIGNAL(clicked()), this, SLOT(unrandomize()));
}

void GraphicalCore::randomizeDir(const QString& path, bool recursive)
{
	QDir dir(path);

	QList<QString> dirContents = dir.entryList(QDir::Files | QDir::Dirs | QDir::NoDot | QDir::NoDotDot);
	for(int pos=0; pos < dirContents.size(); pos++)
	{
		int swapWith = rand() % dirContents.size();
		dirContents.swap(pos,swapWith);
	}
	for(int pos=0; pos < dirContents.size(); pos++)
	{
		QString numStr = QString::number(pos);
		QString numPrefix="";
		while(numStr.size()+numPrefix.size() < QString::number(dirContents.size()).size())
			numPrefix += "0";
		
		QString nFileName = dirContents[pos];
		nFileName.remove(QRegExp("^[0-9]+RAND_"));
		nFileName.prepend(numPrefix + numStr + "RAND_");
		QFile::rename(path+QString("/")+dirContents[pos], path+QString("/")+nFileName); 
	}

	if(recursive)
	{
		QList<QString> subdirs = dir.entryList(QDir::Dirs | QDir::NoDot | QDir::NoDotDot);
		for(int pos=0; pos < subdirs.size(); pos++)
			randomizeDir(path+QString('/')+subdirs[pos], true);
	}
}

void GraphicalCore::unrandomizeDir(const QString& path, bool recursive)
{
	QDir dir(path);

	QList<QString> dirContents = dir.entryList(QDir::Files | QDir::Dirs | QDir::NoDot | QDir::NoDotDot);
	for(int pos=0; pos < dirContents.size(); pos++)
	{
		QString nFileName = dirContents[pos];
		nFileName.remove(QRegExp("^[0-9]+RAND_"));
		QFile::rename(path+QString("/")+dirContents[pos], path+QString("/")+nFileName); 
	}

	if(recursive)
	{
		QList<QString> subdirs = dir.entryList(QDir::Dirs | QDir::NoDot | QDir::NoDotDot);
		for(int pos=0; pos < subdirs.size(); pos++)
			unrandomizeDir(path+QString('/')+subdirs[pos], true);
	}	
}

void GraphicalCore::slotSelectPath()
{
	QString path=QFileDialog::getExistingDirectory(this, tr("Select the directory to randomize"));
	if(path.isEmpty())
		return;

	le_dirpath->setText(path);
}

void GraphicalCore::randomize()
{
	if(le_dirpath->text().isEmpty())
	{
		QMessageBox::warning(this, tr("Error"), tr("You must choose a directory before!"));
		return;
	}

	if(cb_recursive->isChecked())
		randomizeDir(le_dirpath->text(), true);
	else
		randomizeDir(le_dirpath->text(), false);

	QMessageBox::information(this, tr("Done"), tr("The selected directory has been randomized!"));
}

void GraphicalCore::unrandomize()
{
	if(le_dirpath->text().isEmpty())
	{
		QMessageBox::warning(this, tr("Error"), tr("You must choose a directory before!"));
		return;
	}

	if(cb_recursive->isChecked())
		unrandomizeDir(le_dirpath->text(), true);
	else
		unrandomizeDir(le_dirpath->text(), false);

	QMessageBox::information(this, tr("Done"), tr("The random tags have been removed!"));
}

