<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr" sourcelanguage="en">
<context>
    <name>GraphicalCore</name>
    <message>
        <location filename="GraphicalCore.cpp" line="45"/>
        <source>Directory</source>
        <translation>Dossier</translation>
    </message>
    <message>
        <location filename="GraphicalCore.cpp" line="52"/>
        <source>Choose</source>
        <translation>Choisir</translation>
    </message>
    <message>
        <location filename="GraphicalCore.cpp" line="57"/>
        <source>Affect subdirectories</source>
        <translation>Traiter les sous-dossiers</translation>
    </message>
    <message>
        <location filename="GraphicalCore.cpp" line="63"/>
        <source>Randomize</source>
        <translation>Mélanger</translation>
    </message>
    <message>
        <location filename="GraphicalCore.cpp" line="66"/>
        <source>Remove random tags</source>
        <translation>Supprimer les tags aléatoires</translation>
    </message>
    <message>
        <location filename="GraphicalCore.cpp" line="131"/>
        <source>Select the directory to randomize</source>
        <translation>Choisissez le dossier à mélanger</translation>
    </message>
    <message>
        <location filename="GraphicalCore.cpp" line="142"/>
        <location filename="GraphicalCore.cpp" line="158"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="GraphicalCore.cpp" line="142"/>
        <location filename="GraphicalCore.cpp" line="158"/>
        <source>You must choose a directory before!</source>
        <translation>Vous devez choisir un dossier avant !</translation>
    </message>
    <message>
        <location filename="GraphicalCore.cpp" line="151"/>
        <location filename="GraphicalCore.cpp" line="167"/>
        <source>Done</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location filename="GraphicalCore.cpp" line="151"/>
        <source>The selected directory has been randomized!</source>
        <translation>Le dossier choisi a été mélangé avec succès !</translation>
    </message>
    <message>
        <location filename="GraphicalCore.cpp" line="167"/>
        <source>The random tags have been removed!</source>
        <translation>Les tags aléatoires ont bien été supprimés !</translation>
    </message>
</context>
</TS>
