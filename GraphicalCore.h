
/*
 * SOFTWARE:
 * 		MusicRandomizer
 * AUTHOR:
 * 		Theophile BASTIAN 'Tobast' <contact@tobast.fr>
 * ERROR REPORTING:
 * 		<error-report@tobast.fr>
 *
 * DESCRIPTION:
 * 		Small program designed to "randomize" the playing order of music tracks on a player without "random" feature
 * 		Prepends random IDs to each track and folder in order to alter alphabetic order.
 *
 * LICENCE:
 *		GNUGPLv3:
 *
 * 		Copyright (C) 2013 Theophile BASTIAN
 *		This program is free software: you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation, either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEF_GRAPHICALCORE
#define DEF_GRAPHICALCORE

#include <QMainWindow>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QFileDialog>
#include <QGroupBox>
#include <QMessageBox>
#include <QDir>
#include <QFile>
#include <QRegExp>

#include <cstdlib> // rand()

class GraphicalCore : public QMainWindow
{
	Q_OBJECT
	public:
		GraphicalCore(QWidget* parent=NULL);

	private://meth
		void buildWidget();
		void randomizeDir(const QString& path, bool recursive);
		void unrandomizeDir(const QString& path, bool recursive);

	private slots:
		void slotSelectPath();
		void randomize();
		void unrandomize();

	private:
		QWidget* centralArea;
		QVBoxLayout* layoutMain;
		QGroupBox* pathGroupbox;
		QHBoxLayout* layoutPath;
		QLineEdit* le_dirpath;
		QPushButton* pb_selectPath;
		QCheckBox* cb_recursive;
		QPushButton* pb_randomize;
		QPushButton* pb_unrandomize;
};

#endif//DEF_GRAPHICALCORE

